package kienmt.com.notepad.view;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kienmt.com.notepad.R;
import kienmt.com.notepad.adapter.NoteAdapter;
import kienmt.com.notepad.controller.MainController;
import kienmt.com.notepad.controller.NoteContentController;
import kienmt.com.notepad.database.SQLiteDAO;
import kienmt.com.notepad.itf.DialogLongClick;
import kienmt.com.notepad.model.Note;
import kienmt.com.notepad.util.Const;
import kienmt.com.notepad.view.Dialog.SaveNote;

public class MainActivity extends AppCompatActivity implements NoteAdapter.ItemClickListener, DialogLongClick, DatePickerDialog.OnDateSetListener {
    @BindView(R.id.rvListNode)
    RecyclerView rvListNode;
    @BindView(R.id.tvNodata)
    TextView tvNodata;
    @BindView(R.id.tvSearch)
    TextView tvSearch;
    @BindView(R.id.tvFull)
    TextView tvFull;

    private NoteAdapter adapter;
    private List<Note> arrNote;
    private ArrayList<Note> noteCoppy;

    NoteContentController contentController = new NoteContentController();
    MainController mainController = new MainController();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initRecycleView();
    }

    private void initRecycleView() {
        LinearLayoutManager manager = new LinearLayoutManager(this);
        rvListNode.setLayoutManager(manager);

        arrNote = new ArrayList<>();
        adapter = new NoteAdapter(this, arrNote);
        adapter.setClickListener(this);
        rvListNode.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        arrNote.clear();
        arrNote.addAll(SQLiteDAO.createQuery(Note.class).perform());
        if (arrNote.size() > 0) {
            noteCoppy = new ArrayList<>(arrNote);
            tvSearch.setVisibility(View.VISIBLE);
            tvFull.setVisibility(View.VISIBLE);
            tvNodata.setVisibility(View.GONE);
            rvListNode.setVisibility(View.VISIBLE);
            contentController.sortListDate(arrNote);
            adapter.notifyDataSetChanged();
        } else {
            noData();
            tvSearch.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.imvNewNote)
    public void openNewNote() {
        startActivity(new Intent(MainActivity.this, NoteContentActivity.class));
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(MainActivity.this, NoteContentActivity.class);
        intent.putExtra(Const.KEY_NOTE, arrNote.get(position));
        startActivity(intent);
    }

    @Override
    public void onLongClick(int position) {
        SaveNote.showRemoveCase(this, this, position);
    }

    @Override
    public void longClick(int position) {
        Note note = arrNote.get(position);
        if (SQLiteDAO.removeObjectWithID(note.getId(), Note.class.getSimpleName())) {
            arrNote.remove(position);
            adapter.notifyDataSetChanged();
            Toast.makeText(this, getResources().getString(R.string.remove_note_success), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, getResources().getString(R.string.remove_note_failed), Toast.LENGTH_LONG).show();
        }
        if (arrNote.size() == 0) {
            noData();
        }
    }

    private void noData() {
        tvNodata.setVisibility(View.VISIBLE);
        rvListNode.setVisibility(View.GONE);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        // set day of month , month and year value in the edit text
        String getDateForPicker = (monthOfYear + 1) + "/"
                + dayOfMonth + "/" + year;

        arrNote.clear();

        for (Note item : noteCoppy) {
            if (item.getDateCreate().contains(getDateForPicker)) {
                arrNote.add(item);
            }
        }

        if (arrNote.size() == 0) {
            arrNote.addAll(noteCoppy);
            Toast.makeText(MainActivity.this, getResources().getString(R.string.search_no_result), Toast.LENGTH_LONG).show();
        }
        adapter.notifyDataSetChanged();
    }

    @OnClick(R.id.tvSearch)
    public void openPickDate() {
        mainController.openDateCMND(this, this);
    }

    @OnClick(R.id.tvFull)
    public void fullNote() {
        arrNote.clear();
        arrNote.addAll(noteCoppy);
        adapter.notifyDataSetChanged();
    }
}
