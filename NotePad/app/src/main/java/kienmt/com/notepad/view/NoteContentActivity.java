package kienmt.com.notepad.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kienmt.com.notepad.R;
import kienmt.com.notepad.controller.NoteContentController;
import kienmt.com.notepad.database.SQLiteDAO;
import kienmt.com.notepad.itf.DialogLongClick;
import kienmt.com.notepad.itf.DialogOnlick;
import kienmt.com.notepad.model.Note;
import kienmt.com.notepad.util.BackgroundDef;
import kienmt.com.notepad.view.Dialog.SaveNote;

public class NoteContentActivity extends AppCompatActivity implements DialogOnlick,DialogLongClick {
    @BindView(R.id.rlParent)
    RelativeLayout rlParent;
    @BindView(R.id.edtContent)
    EditText edtContent;
    @BindView(R.id.imvRemove)
    ImageView imvRemove;

    private Note note;
    private NoteContentController ncController = new NoteContentController();
    private int codeColor;
    private boolean isUpdate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_content);
        ButterKnife.bind(this);

        codeColor = BackgroundDef.COLOR_YELLOW;

        note = ncController.getNote(getIntent().getExtras());

        //xem chi tiết
        if (ncController.isCheckNoteExits(note)) {
            isUpdate = true;
            imvRemove.setVisibility(View.VISIBLE);
            edtContent.setText(note.getContent());
            switch (note.getColor()) {
                case BackgroundDef.COLOR_YELLOW:
                    changeBgYellow();
                    break;
                case BackgroundDef.COLOR_GREEN:
                    changeBgGreen();
                    break;
                case BackgroundDef.COLOR_BLUE:
                    changeBgBlue();
                    break;
                case BackgroundDef.COLOR_BUBLE:
                    changeBgBuble();
                    break;
                case BackgroundDef.COLOR_PINK:
                    changeBgPink();
                    break;
                case BackgroundDef.COLOR_WHITE:
                    changeBgWhite();
                    break;
            }
        }
    }


    @OnClick({R.id.imvCircleBlue})
    public void changeBgBlue() {
        codeColor = BackgroundDef.COLOR_BLUE;
        rlParent.setBackgroundColor(getResources().getColor(R.color._afe0ec));
    }

    @OnClick({R.id.imvCircleBuble})
    public void changeBgBuble() {
        codeColor = BackgroundDef.COLOR_BUBLE;
        rlParent.setBackgroundColor(getResources().getColor(R.color._cfbcdc));
    }

    @OnClick({R.id.imvCircleGreen})
    public void changeBgGreen() {
        codeColor = BackgroundDef.COLOR_GREEN;
        rlParent.setBackgroundColor(getResources().getColor(R.color._c4e2b8));
    }

    @OnClick({R.id.imvCirclePink})
    public void changeBgPink() {
        codeColor = BackgroundDef.COLOR_PINK;
        rlParent.setBackgroundColor(getResources().getColor(R.color._f4c0cb));
    }

    @OnClick({R.id.imvCircleWhite})
    public void changeBgWhite() {
        codeColor = BackgroundDef.COLOR_WHITE;
        rlParent.setBackgroundColor(getResources().getColor(R.color._ffffff));
    }

    @OnClick({R.id.imvCircleYellow})
    public void changeBgYellow() {
        codeColor = BackgroundDef.COLOR_YELLOW;
        rlParent.setBackgroundColor(getResources().getColor(R.color._faf6bd));
    }

    @OnClick({R.id.imvRemove})
    public void removeNote() {
        SaveNote.showRemoveCase(this, this, 0);
    }

    @Override
    public void onClick(Note note) {
        if (isUpdate) {
            SQLiteDAO.update(note);
            Toast.makeText(this, getResources().getString(R.string.note_success_update), Toast
                    .LENGTH_LONG)
                    .show();
        } else {
            SQLiteDAO.insertOrUpdate(note);
            Toast.makeText(this, getResources().getString(R.string.note_success), Toast.LENGTH_LONG)
                    .show();
        }
        finish();
    }

    @OnClick(R.id.imvSave)
    public void saveNote() {
        String dateNow = ncController.getDateFormat("MM/dd/yyyy HH:mm:ss");
        note.setDateCreate(dateNow);
        note.setColor(codeColor);
        note.setContent(edtContent.getText().toString().trim().equals("") ? "" : edtContent
                .getText().toString().trim());
        SaveNote.showCreateCase(this, this, note);
    }

    @Override
    public void longClick(int position) {
        if (SQLiteDAO.removeObjectWithID(note.getId(), Note.class.getSimpleName())) {
            Toast.makeText(this, getResources().getString(R.string.remove_note_success), Toast.LENGTH_LONG).show();
            finish();
        } else {
            Toast.makeText(this, getResources().getString(R.string.remove_note_failed), Toast.LENGTH_LONG).show();
        }
    }
}
