package kienmt.com.notepad.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

import kienmt.com.notepad.database.annotation.PrimaryKey;

public class Note implements Serializable {
    @PrimaryKey
    int id;

    String title;
    String content;
    String dateCreate;
    int color;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(String dateCreate) {
        this.dateCreate = dateCreate;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
