package kienmt.com.notepad.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import kienmt.com.notepad.R;
import kienmt.com.notepad.model.Note;
import kienmt.com.notepad.util.BackgroundDef;

public class NoteAdapter extends RecyclerView.Adapter<
        NoteAdapter.DetailsViewHolder> {
    List<Note> lvNote;
    Context context;
    LayoutInflater inflater;
    private ItemClickListener mClickListener;

    public NoteAdapter(Context ct, List<Note> _lvNote) {
        this.context = ct;
        this.lvNote = _lvNote;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public DetailsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_note, parent, false);
        return new DetailsViewHolder(view);
    }


    @Override
    public void onBindViewHolder(DetailsViewHolder holder, int position) {
        Note note = lvNote.get(position);
        holder.tvTitle.setText(note.getTitle());
        holder.tvContent.setText(note.getContent());
        holder.tvDate.setText(note.getDateCreate());

        int type = note.getColor();
        switch (type) {
            case BackgroundDef.COLOR_YELLOW:
                holder.bgTag.setBackgroundColor(context.getResources().getColor(R.color._f7e712));
                break;
            case BackgroundDef.COLOR_GREEN:
                holder.bgTag.setBackgroundColor(context.getResources().getColor(R.color._75d64c));
                break;
            case BackgroundDef.COLOR_BLUE:
                holder.bgTag.setBackgroundColor(context.getResources().getColor(R.color._63d0de));
                break;
            case BackgroundDef.COLOR_BUBLE:
                holder.bgTag.setBackgroundColor(context.getResources().getColor(R.color._ac84d3));
                break;
            case BackgroundDef.COLOR_PINK:
                holder.bgTag.setBackgroundColor(context.getResources().getColor(R.color._ef8ba1));
                break;
            case BackgroundDef.COLOR_WHITE:
                holder.bgTag.setBackgroundColor(context.getResources().getColor(R.color._d5d5d3));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return lvNote.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class DetailsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView tvTitle;
        TextView tvContent;
        TextView tvDate;
        View bgTag;

        public DetailsViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvContent = itemView.findViewById(R.id.tvContent);
            tvDate = itemView.findViewById(R.id.tvDateCreate);
            bgTag = itemView.findViewById(R.id.viewType);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null)
                mClickListener.onItemClick(getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            if (mClickListener != null)
                mClickListener.onLongClick(getAdapterPosition());
            return false;
        }
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(int position);

        void onLongClick(int position);
    }
}
