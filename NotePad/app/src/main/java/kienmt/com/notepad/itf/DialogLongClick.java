package kienmt.com.notepad.itf;

import kienmt.com.notepad.model.Note;

public interface DialogLongClick {
    void longClick(int position);
}
