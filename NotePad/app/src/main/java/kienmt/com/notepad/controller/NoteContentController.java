package kienmt.com.notepad.controller;

import android.os.Bundle;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import kienmt.com.notepad.model.Note;
import kienmt.com.notepad.util.Const;

public class NoteContentController {

    public String getDateFormat(String fmDate) {
        Date currentTime = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat(fmDate);
        String formattedDate = df.format(currentTime);
        return formattedDate;
    }

    public void sortListDate(List<Note> arrNote) {
        Collections.sort(arrNote, new Comparator<Note>() {
            public int compare(Note noteFirst, Note noteTwo) {
                try {
                    Date a = toDate(noteFirst.getDateCreate());
                    Date b = toDate(noteTwo.getDateCreate());
                    return b.compareTo(a);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return -1;
            }
        });
    }

    public Note getNote(Bundle bundle) {
        if (bundle != null) {
            Note note = (Note) bundle.getSerializable(Const.KEY_NOTE);
            return note;
        }
        return new Note();
    }

    public boolean isCheckNoteExits(Note note) {
        if (note.getId() > 0) {
            return true;
        }
        return false;
    }

    public static Date toDate(String value) throws ParseException {
        DateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        return format.parse(value);
    }
}
