package kienmt.com.notepad.util;

import java.util.ArrayList;
import java.util.Arrays;

public class StringUtil {
    public static String cutExtendImage(String fullString) {
        String[] result = fullString.split("\\.");
        return result[0];
    }

    public static ArrayList<String> formatChildMap(String key, String fullString) {
        String[] result = fullString.split(key);
        return new ArrayList<String>(Arrays.asList(result));
    }
}
