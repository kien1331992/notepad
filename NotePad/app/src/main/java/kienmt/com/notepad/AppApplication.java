package kienmt.com.notepad;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

public class AppApplication extends Application {
    private static AppApplication mSelf;

    public static AppApplication self() {
        return mSelf;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mSelf = this;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
