package kienmt.com.notepad.view.Dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import kienmt.com.notepad.R;
import kienmt.com.notepad.itf.DialogLongClick;
import kienmt.com.notepad.itf.DialogOnlick;
import kienmt.com.notepad.model.Note;

public class SaveNote {

    public static void showCreateCase(final Activity mContext, final DialogOnlick even, final Note note) {
        if (!mContext.isFinishing()) {
            final AlertDialog builder = new AlertDialog.Builder(mContext).create();
            builder.show();
            builder.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
            builder.setCanceledOnTouchOutside(false);
            builder.setCancelable(false);

            Window window = builder.getWindow();
            window.setContentView(R.layout.custom_dialog_save_note);

            RelativeLayout btn_ok = window.findViewById(R.id.rlAccept);
            RelativeLayout btn_cancel = window.findViewById(R.id.rlCancel);
            final EditText edt_title = window.findViewById(R.id.edtTitle);

            if(!TextUtils.isEmpty(note.getTitle())){
               edt_title.setText(note.getTitle());
            }
            btn_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    builder.dismiss();
                }
            });

            btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    note.setTitle(edt_title.getText().equals("") ? mContext.getResources().getString(R.string.no_title) : edt_title.getText().toString().trim());
                    even.onClick(note);
                    builder.dismiss();
                }
            });
        }
    }

    public static void showRemoveCase(final Activity mContext, final DialogLongClick even, final int position) {
        if (!mContext.isFinishing()) {
            final AlertDialog builder = new AlertDialog.Builder(mContext).create();
            builder.show();
            builder.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
            builder.setCanceledOnTouchOutside(false);
            builder.setCancelable(false);

            Window window = builder.getWindow();
            window.setContentView(R.layout.custom_dialog_confirm);

            Button btn_ok = window.findViewById(R.id.btn_ok);
            Button btn_cancel = window.findViewById(R.id.btn_cancel);


            btn_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    builder.dismiss();
                }
            });

            btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    even.longClick(position);
                    builder.dismiss();
                }
            });
        }
    }
}
