package kienmt.com.notepad.util;

import android.support.annotation.IntDef;
import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@IntDef({BackgroundDef.COLOR_YELLOW, BackgroundDef.COLOR_GREEN, BackgroundDef.COLOR_BLUE, BackgroundDef.COLOR_BUBLE, BackgroundDef.COLOR_PINK, BackgroundDef.COLOR_WHITE})
@Retention(RetentionPolicy.SOURCE)
public @interface BackgroundDef {
    int COLOR_YELLOW = 0;
    int COLOR_GREEN = 1;
    int COLOR_BLUE = 2;
    int COLOR_BUBLE = 3;
    int COLOR_PINK = 4;
    int COLOR_WHITE = 5;
}
