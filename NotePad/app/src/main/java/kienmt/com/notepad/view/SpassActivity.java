package kienmt.com.notepad.view;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

import kienmt.com.notepad.R;
import kienmt.com.notepad.database.SQLiteDAO;
import kienmt.com.notepad.util.Const;
import kienmt.com.notepad.util.SharedPrefs;

public class SpassActivity extends AppCompatActivity {
    SharedPrefs sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spass);

        sp = SharedPrefs.getInstance();
        checkPermission();
    }

    private void checkPermission() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            Dexter.withActivity(SpassActivity.this)
                    .withPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest
                            .permission.READ_EXTERNAL_STORAGE)
                    .withListener(new MultiplePermissionsListener() {
                        @Override
                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                            if (report.areAllPermissionsGranted()) {
                                // do you work now
                                checkInstallDb();
                            } else {
                                finish();
                            }
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest>
                                                                               permissions,
                                                                       PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    })
                    .check();
        } else {
            checkInstallDb();
        }
    }

    private void checkInstallDb() {
        String username = sp.get(Const.KEY_INSTALL_APP, String.class);
        if ("".equals(username)) {
            SharedPrefs.getInstance().put(Const.KEY_INSTALL_APP, "");
            createDb();
        }

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                startActivity(new Intent(SpassActivity.this, MainActivity.class));
                finish();
            }
        }, 1000);

    }

    private void createDb() {
        SQLiteDAO.initialize(this);
        if (!SQLiteDAO.checkDB()) {
            SQLiteDAO.createDB();
        }
    }
}
