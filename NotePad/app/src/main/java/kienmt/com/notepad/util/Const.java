package kienmt.com.notepad.util;

public class Const {
    public static final String EXITS_DB = "isDbNote";

    public static final String KEY_INSTALL_APP = "isInstallNoteApp";
    public static final String KEY_NOTE = "note";
}
